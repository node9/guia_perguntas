//imports
const express = require('express');
const app = express();

//habilitando ejs
app.set('view engine', 'ejs');
//habilitando arquivos estaticos(css, imgs)
app.use(express.static('public'));

//rotas
app.get('/:nome/:lang', (req, res) => {
    res.render('index');
});

//starting server
app.listen(8080, (error) => {
    if(error) {
      console.log("App rodando!");
    } else {
      console.log("Servidor iniciado com sucesso!");  
    }
});